//instalar npm i passport-local bcrypt
const LocalStrategy = require('passport-local').Strategy;

const bcrypt = require('bcrypt');

const User = require('../models/User');

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return re.test(String(password));
}


const registerStrategy = new LocalStrategy(
    {
        usernameField:"email",
        passwordField:"password",
        passReqToCallback: true
    },

    async(req,email,password,done)=>{

        try{

            const existingUser = await User.findOne({email});

            if(existingUser){
                const error = new Error('Usuario ya registrado');
                return done(error);
            }

            const isValidEmail = validateEmail(email);

            if(!isValidEmail){
                const error = new Error('Email incorrecto. No cumple formato');
                return done(error);
            }

            const isValidPassword = validatePass(password);

            if(!isValidPassword){
                const error = new Error('Contraseña incorrecta. Minimo se necesita 6 caracteres, 1 minúscula, 1 mayúscula y 1 número');
                return done(error);
            }

            const saltRounds = 10;

            const hash = await bcrypt.hash(password,saltRounds);

            const newUser = new User(
                {
                    email,
                    password: hash,
                    name:req.body.name,
                }
            );

            const savedUser = await newUser.save();
            //IMPORTANTE
            savedUser.password = undefined;
            
            //como ya no hay errores le pasamos el primer parametro de error como null
            return done(null,savedUser)

        }catch(error){
            return done(error);
        }

    }
);

module.exports = registerStrategy;
