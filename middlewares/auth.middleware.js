const isAuth = (req,res,next) => {

    console.log(req.isAuthenticated())

    if(req.isAuthenticated()){
        return next();
    }

    return res.redirect('/auth/login');

}

module.exports = isAuth;