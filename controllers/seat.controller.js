const seatGet =  async(req,res,next)=>{
    console.log('seatGet')
    return res.send('Controller Seat get ');
};

const seatPost =  async(req,res,next)=>{
    console.log('seatPost')
    return res.send('Controller Seat post');
};

const seatPut =  async(req,res,next)=>{
    console.log('seatPut')
    return res.send('Controller Seat put');
};

const seatDelete =  async(req,res,next)=>{
    console.log('seatDelete')
    return res.send('Controller Seat delete');
};


module.exports = {
    seatGet,
    seatPost,
    seatPut,
    seatDelete
};