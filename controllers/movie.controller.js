const passport = require("passport");
const Movie = require("../models/Movies");

const moviesGet = async (req, res) => {
  try {
    const movies = await Movie.find();
    return res.status(200).json(movies);

    // return res.status(200).render('./movies/movies',{title:'Movies',movies});
  } catch (error) {
    return res.status(500).json(error);
  }
};

const moviePost = async (req, res, next) => {
  try {
    const { title, director, description, duration, genere } = req.body;

    const newMovie = new Movie({
      title,
      director,
      description,
      duration,
      genere,
    });

    //guardando en la base de datos
    const createMovie = await newMovie.save();


    return res.status(200).json(createMovie);

    // return res.redirect(`/movies/${createMovie._id}`);
  } catch (error) {
    const myError = new Error("[Error] no se ha podido añadir la pelicula");

    return next(myError);
  }
};

const movieGetById = async (req, res) => {
  const { id } = req.params;

  try {
    const movie = await Movie.findById(id);
    console.log(movie);
    // return res.status(200).render("./movies/movie", { movie });
    return res.status(200).json(movie);
    // if(movie){
    //     return res.status(200).json(movie);
    // }else{
    //     return res.status(404).json(`No se encuentra pelicula con esta ID`);
    // }
  } catch (error) {
    console.log("Error buscando peliculas->", error);

    return res
      .status(500)
      .json("No se ha podido encontrar la película que buscas");
  }
};

const movieGetByName = async (req, res) => {
  const { name } = req.params;

  try {

    const movies = await Movie.find({title:{'$regex' : name, '$options' : 'i'}})

    if (movies.length === 0) {
      const error = new Error("Pelicula no encontrada");
      throw error;
    }

    return res.status(200).json(movies);

  } catch (error) {
    console.log("Error buscando peliculas->", error);

    return res
      .status(500)
      .json("No se ha podido encontrar la película que buscas");
  }
};

const moviePut = async (req, res, next) => {
  try {
    const { id, title, director, description, duration, genere } = req.body;

    //que utilidad tiene el objeto update
    const update = {};

    if (title) update.title = title;
    if (director) update.director = director;
    if (description) update.description = description;
    if (duration) update.duration = duration;
    if (genere) update.genere = genere;

    const updateMovie = await Movie.findByIdAndUpdate(
      id,
      update,
      { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
    );
    return res.status(200).json(updateMovie);
  } catch (error) {
    const myError = new Error("[Error] no se ha podido modificar los datos");

    return next(myError);
  }
};

const movieDelete = async (req, res, next) => {
  console.log("delete-->");
  const { id } = req.params;

  try {
    const deleted = await Movie.findByIdAndDelete(id);
    console.log(deleted);

    if (!deleted) {
      return res.status(404).json("El elemento que querías borrar no existe");
    } else {
        return res.status(200).json("El elemento ha sido borrado");
    }
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  moviesGet,
  moviePost,
  movieGetById,
  movieGetByName,
  moviePut,
  movieDelete,
};
