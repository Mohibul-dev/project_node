const express = require("express");

const path = require("path");
const methodOverride = require('method-override');

const movieRoutes = require('./routes/Movie.routes');
const seatRoutes = require('./routes/Seat.routes');
const authRoutes = require('./routes/Auth.routes');

//para el auth instalar --> express-session y connect-mongo
const auth = require('./auth');
const session  = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");


//requerimos el archivo db.js para su posterior uso
const db = require('./db.js');

//estrategias de inicio de sesion
auth.setStrategies();

db.connect();

const PORT = 3000;
const app = express();

//para el cookies
app.use(session({
  secret:"asdna½¡n~#ASa!sxcas¿1@",
  resave:false,
  saveUninitialized:false,
  cookie:{
    maxAge:24 * 60 * 60 * 1000,
  },
  store: MongoStore.create({mongoUrl:db.DB_URL}),
}));

//configuramos a express para que use el passport
app.use(passport.initialize());

//para tener el usuario en cualquier lado
app.use(passport.session());


app.use((req, res, next) => {
  // console.log('middleware propio');
  req.isAuth = req.isAuthenticated();
  next();
});




/**
 * Estas son funciones propias de express que transforman la información enviada 
 * como JSON al servidor de forma que podremos obtenerla en req.body.
 */

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use(methodOverride('_method'));

/**
 * configuracion de handlebars
 */
//le dice a express en que carpeta se encuentra las vistas
app.set('views',path.join(__dirname, 'views'));
//le decimos a express que vamos a trabajar con hbs
app.set('view engine','hbs');


const router = express.Router();


router.get("/", (req, res) => {
    const msg = 'Home'
    return res.render('index',{title:'Ruta base con HBS',msg})
});


app.use("/", router);

app.use('/movies',movieRoutes);
app.use('/seat',seatRoutes);

app.use('/auth',authRoutes);

app.use('*',(req,res,next) => {

  const error = new Error('Ruta no encontrada')
  
  return res.status(404).json(error.message);

})

//para controlar la ruta de error
app.use((error,req, res, next) => {
  console.log("index.js-->",error);
  return res.status(error.status || 500).json(error.message || 'Unexpected error');
})


app.listen(PORT, () => {
  console.log(`Servidor running in http://localhost:${PORT}`);
});