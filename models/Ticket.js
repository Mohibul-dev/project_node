const mongoose = require('mongoose');

const {Schema} = mongoose;

const ticketSchema = new Schema (
    {
        hasPaid = {type: Boolean,required: true},
        counts:{type:Number,required: true},
        positions:{type:mongoose.Types.ObjectId,ref:'Seat'},
    },
    {timestamps: true}
);

const Ticket = mongoose.model('Tickets',ticketSchema);

module.exports = Ticket;