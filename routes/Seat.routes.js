const express = require('express');
const router = express.Router();
const  isAuth  = require('../middlewares/auth.middleware');
const controller = require('../controllers/seat.controller');


router.get('/',controller.seatGet);

router.post('/creat',controller.seatPost);

router.put('/edit',controller.seatPut);

router.delete('/delete',controller.seatDelete);

module.exports = router;