const express = require('express');
const passport = require('passport');


const router = express.Router();

router.get('/register',(req,res,next) => {
    //vista para el formulario de registros
    return res.render("./auth/register");
});

router.post('/register',async(req, res, next) => {
    //recibe el parametro de la estrategia creada del index (auth) --> registerStrategy

    const done = (error,user) => {
        if(error) {
            return next(error);
        }
        console.log("Usuario registrado",user);

        return res.json(user);
    }

    passport.authenticate("registerStrategy",done)(req);

    
});

router.get('/login',(req, res, next) => {
    //vista para el formulario de registros
    return res.render("./auth/login");
});

router.post('/login',async(req, res, next) => {
    //recibe el parametro de la estrategia creada del index (auth) --> loginStrategy

    const done = (error, user) => {
        if(error) {
            return next(error);
        }

        req.logIn(user,(error)=>{
            if(error) {
                return next(error);
            }
            
            console.log("Usuario logueado",user);
            return res.json(user); //-->juan tiene redirect

        })
    };
    passport.authenticate("loginStrategy",done)(req);
    
});

router.post("/logout", (req, res, next) => {
    if (req.user) {
      req.logout();
      
      req.session.destroy(() => {
        res.clearCookie("connect.sid");
        return res.redirect("/");
      });
    }
});

module.exports = router;