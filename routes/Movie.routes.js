const express = require('express');
const router = express.Router();
const  isAuth  = require('../middlewares/auth.middleware');
const controller = require('../controllers/movie.controller');



router.get('/',controller.moviesGet);


router.post('/create',controller.moviePost);

router.get('/:id',controller.movieGetById);

router.get('/title/:name',controller.movieGetByName);


router.put('/edit',controller.moviePut)

router.delete("/delete/:id",controller.movieDelete);






module.exports = router;