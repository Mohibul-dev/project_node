const mongoose = require ('mongoose');

//URL local de nuestra BBDD en mongoose y el nombre sera "test1" 
//si no existe la crea
const DB_URL = 'mongodb://localhost:27017/Cine'
// const dotenv = require('dotenv');

//funcion que se conecta a nuestro servidor de manera asincrona
const connect = async() => {

    try{
        await mongoose.connect(DB_URL, {
        
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        
        })
        console.log('Conectado a la base de datos');
    
    }catch(error){
        console.log(error);
    }

}

module.exports = { DB_URL, connect };
